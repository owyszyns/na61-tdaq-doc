![pteroDAQtyl](img/pteroDAQtyl_logo.png)
**pteroDAQtyl** is a software framework enabling a uniform way of running the TDAQ processes. Each TDAQ process must obey the rules of the defined Finite State Machine. All the processes are controlled by a Core Application using a [Supervisor](http://supervisord.org/) system with a Web-based interface. Each process creates the instance of the ModuleManager and CommandHandler. The ModuleManager can load a configurable number of Modules. The pteroDAQtyl Module is a class inheriting from the DAQModule class and defining the following functions: start()/stop(), configure()/unconfigure(), runner(), etc. The runner() function defines the code which is executed in the loop when the Module is running. Modules can communicate between each other (sending the sub-events) either through a local queue by passing the pointer to the event object (modules running in the same process) or by sending the event using ZeroMQ via the DataManager. To send the events, Modules can use either TCP/IP over ethernet or shared memory Inter Process Communication (IPC).

# Installation
## UBuntu
### Dependencies
```bash
sudo apt-get install git cmake g++
```

### cmake installation

The minimum required cmake version is 3.20.

The instruction how to manually update you cmake can be found here:

<https://askubuntu.com/questions/355565/how-do-i-install-the-latest-version-of-cmake-from-the-command-line>

### ZMQ installation

[ZMQ](https://zeromq.org/) is an asynchronous messaging library, aimed at use in distributed or concurrent applications.


```bash
mkdir zmq
cd zmq
```
Might be needed:
```
sudo apt-get install pkg-config
```

Install libzmq v4.3.4
```bash
git clone --depth 1 --branch v4.3.4 https://github.com/zeromq/libzmq.git
cd libzmq
mkdir build
cd build
cmake ..
sudo make -j4 install
```

Install zmqcpp v4.7.0
```bash
git clone --depth 1 --branch v4.7.0 https://github.com/zeromq/cppzmq.git
cd cppzmq
mkdir build
cd build
cmake ..
sudo make -j4 install
```

### Supervisor configuration

#### a) if you want to run it as deamon process:

- install supervisor:
``` bash
sudo apt-get install supervisor
```
- configure python supervisor_twiddler (has to be installed for default python):
``` bash
sudo apt-get install python-pip
sudo pip install supervisor_twiddler
```
- configure Supervisor:

add to file: sudo vim /etc/supervisor/supervisord.conf:

```
[inet_http_server]
port=*:9001

[rpcinterface:twiddler]
supervisor.rpcinterface_factory = supervisor_twiddler.rpcinterface:make_twiddler_rpcinterface

[group:daq]
```
- restart deamon process
``` bash
sudo systemctl restart supervisor
```
#### b) if you want to run it manually (no root permissions needed except for apt-get)

- install python virtualenv:
``` bash
sudo apt-get install python3-venv
```
- create virtual environment and activate:
``` bash
python3 -m venv python3env
source python3env/bin/activate
```
- configure python supervisor_twiddler
``` bash
pip install supervisor_twiddler
```
- install Supervisor
``` bash
sudo apt-get install supervisor
sudo systemctl stop supervisor (if needed)
```
- copy the configuration file: [supervisor.conf](txt/supervisord.conf)

- start supervisor:
``` bash
supervisord -c supervisord.conf -l supervisor.log
```
### pteroDAQtyl installation
``` bash
git clone https://gitlab.cern.ch/na61-software/online/pterodaqtyl/pterodaqtyl.git
cd pterodaqtyl
mkdir build
cd build
cmake ../
make -j<n>
```


# Modules

The *pteroDAQtyl* building block is the "Module", a plugin that can be loaded at run-time by the bare `pteroDAQtyl` executable, which then acquires its functionalities.

The bare `pteroDAQtyl` executable (or "Core") includes the plugin loading mechanism, configuration, and logging.

A Module is a class, inheriting a set of default methods from the `DAQModule` base class, such as the `runner()` or `start(int run_num)` / `stop()`.

The Module developer can extend its functionalities by defining specific behavior for the default methods and add methods.


# Configuration

A pteroDAQtyl JSON configuration is composed of any number of "processes".

A process is a `pteroDAQtyl` executable with a Module(s) loaded. An example of the standard JSON configuration for one component is:

```json
{
  "name": "R001",
  "host": "localhost",
  "command port": 5555,
  "dir" : "/home/wojtek/NA61Soft/pterodaqtyl/",
  "exe" : "build/bin/pteroDAQtyl_main",
  "user": "wojtek",
  "modules": [
    {
      "name" : "SubEventBuilder01",
      "module" : "SubEventBuilder",
      "settings": {"board_id": 0, "nodes": 1},
      "metrics": {"host": "localhost", "port": 6007, "interval": 500}
    },
    {
      "name" : "SubEventReaderSim01",
      "module" : "SubEventReaderSim",
      "settings": {"board_id": 0, "delay_us": 1250},
      "metrics": {"host": "localhost", "port": 6007, "interval": 500}
    }
  ]
}

```
* `"name":` name of the process for Supervisor
* `"host":` defines the machine where the process is spawne
* `"command port":` is used for the command server of the component. Configuration and commands are sent to this socket
* `"dir":` the home directory of pteroDAQtyl project
* `"user":` user who executes the process
* `"modules":` array of modules that will be loaded
* `"module":` is the exact name (case sentitive) of the Module to be loaded.
* `"settings":` include all configuration parameters that are specific to the specified Module
* `"metrics":` contains the configuration for the metrics manager

# Monitoring

## Metrics registration

Each Module has its own instance of "MetricManager" class (*m_metric_manager*) which provides the interface for metrics monitoring. While defining your custom Module you can register the metric using the template function:

```cpp
void registerMetric(T* pointer, std::string name, metrics::metric_type mtype, float delta_t=1)
```
       
in the `configure()` function of your Module, where:

* pointer - pointer to the variable storing the metric value
* name - name of the metric published to database
* mtype - type of the metric
* delta_t - time interval between measurements in seconds

For example:

    m_statistics->registerMetric<std::atomic<int> >(&m_metric6, "RandomMetric1-int", daqling::core::metrics::LAST_VALUE);
 
The acceptable types of variables are:

* `std::atomic<int>`
* `std::atomic<float>`
* `std::atomic<double>`
* `std::atomic<bool>`
* `std::atomic<size_t>`

There are 4 possible metrics types:

* `LAST_VALUE` - measure the current value of metrics
* `ACCUMULATE` - accumulate the current variable value to metric value and reset the variable
* `AVERAGE` - calculate the average value of metric over given time interval
* `RATE` - calculate the rate over given time interval

## Metrics visualization
The metrics values are published via ZMQ socket. The example subscriber can be found here:

```
scripts/Monitoring/metric-manager.py
```

with a JSON configuration file: `metrics-config.json`.

This subscriber gives the possibility to send the metrics values to Redis or/and InfluxDB. It is also possible to set the list of subscribed metrices as well as the individual destination of defined metric.

The default metrics visualization tool is [Grafana](https://grafana.com/) connected to [influxDB](https://www.influxdata.com).

###Monitoring configuration for InfluxDb 1.8
Download and install influxDB - select version 1.8:
<https://portal.influxdata.com/downloads/>

Start influxDb:
```
sudo systemctl start influxdb
```

Create database:
You can use the python script:
```
/scripts/Monitoring/create-database.py
```
To run (it creates a default database with name pteroMetrices):
```
pip install influxdb
python create-database.py
```

Install and configure Grafana:
<https://grafana.com/grafana/download>

Enable and start grafana:
```
sudo systemctl enable grafana-server
sudo systemctl start grafana-server
```
To configure Grafana visit (default login: admin, password: admin):
```
http://<host>:3000
```

Add data source:
```
Settings -> Configuration -> Data Source -> Add Data Source
```
As the url put the default influxDB configuration: http://localhost:8086 and our default DB name: pteroMetrices 

Click Save&Test

Run the examples: [instructions](#run-the-examples)

Start metric subscriber:
```
cd scripts/Monitoring
pip install redis
python metric-manager.py metrics-config.json
```

Now metrices values are sent to the influxDB and using Grafana Web GUI you can create new Dashboard. 
###Monitoring configuration for InfluxDb 2.0 (not fully integrated yet. Please use influxDB 1.8)
Download and install influxDB:
<https://portal.influxdata.com/downloads/>

To configure influxDB run:
```
influx setup
```

Install and configure Grafana:
<https://docs.influxdata.com/influxdb/v2.0/tools/grafana/>

Configure python environment:
```
pip install influxdb-client
```

#Data Manager
TBD

#Create your own module
You can create the module scheleton here (currently works with python2 only):
```
scripts/CreateModuleSkeleton/
```
Then copy the created folder to:
```
src/Modules/
```
and rebuild the project. Module will be automatically included in the compilation by cmake.

##Gitlab for modules
Modules should be stored in a dedicated repository and attached to the Core project as git [submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules). To add the git submodule type:
```
git submodule add <path to git repo> src/Modules/<module name>
```
or for Utilities:
```
git submodule add <path to git repo> src/Utilities/<module name>
```
When cloning the project you should at first clone the Core application:
```
git clone https://gitlab.cern.ch/na61-software/online/pterodaqtyl/pterodaqtyl.git
```
the activate and clone the selected git submodules:
```
git submodule init src/Modules/<module name>
git submodule update src/Modules/<module name>
```
# Run the examples
To run the examples the python3 is required.

The usage of python virtual env is recommended:

- install python virtualenv:
``` bash
sudo apt-get install python3-venv
```
- create virtual environment and activate:
``` bash
python3 -m venv python3env
source python3env/bin/activate
```

Running the examples:
``` bash
pip3 install jsonschema
pip3 install zmq
cd /build/scripts/Control/
mkdir log
```
Run the example
``` bash
python3 daqinterface.py config.json
```

To start the modules, follow the FSM commands:
```
            ---add-->         ---load-->            --config-->          --start-->
(not_added)           (added)             (standby)              (ready)            (running)
            <-remove-         <--exit---            <-unconfig-          <--stop--- 

```

##Examples
There are two exmaple config files in:
```
build/scripts/Control/
```
The first one `config.json` includes the configuration for three processes. Each process loads single module. In this example there is one ReaderSim module which simulates dummy sub-events and sends them to EventBuilder module. Orchestrator module takes care about synchronization of the data flow.

The second example `config_subevents.json` loads three modules into one process. The dummy sub-events from SubEventReaderSim modules are passed to SubEventBuilder module via the common events queue provided by the DataManager.
