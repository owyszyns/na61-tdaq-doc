# Configuration before data-taking

## Event Server for Event Browser
To start the Event Server login to na61es:
```bash
ssh na61es@na61es
```
pswd: se16an

Start screen session:
```
screen -S EventServer
```
Run:
```
cd /home/na61es/pterodaqtyl/pterodaqtyl/build/scripts/Control
python3 daqinterface.py config.json
```
Type:
```
load
```
press Enter, then:
```
config
```
press Enter and then:
```
start 1
```
(for details see pteroDAQtyl tab, section: Run the examples).

The logs are here: /home/na61es/pterodaqtyl/pterodaqtyl/build

In case of problems (Event Browser is not refreshing), go to the latest log file and check if it is refreshing. If not, open screen session and restart EventServer application.

## TPC readout

Before the run, restart all the TPC read-out nodes (long running can lead to corrupted data from C-RORCs).

Mount NFS in all TPC nodes (from time to time make sure that NFS is mounted, otherwise pedestal files will not be updated):
```
cd /soft/tdaq/readouttools/TPC/Scripts
./runOnTpcNodes.sh MountNFS
```

The readout configuration is here:
```
/soft/tdaq/readouttools/detector-config/tpc/datataking-config.json
```
Readout parameters have to be adjusted and modified before each data-taking. Update the following json entries:

* zero_suppression_threshold: threshold for zero suppression in RCUs in ADC counts,
* sampling_frequency: set sampling frequency (0=10MHz=default, 1=5MHz, 2=2.5MHz, 3=20MHz),
* numer_of_timeslices: use this number of samples per channel,
* glitch_filter_setting: glitch filter for zero suppression,
* nodes: list of TPC read-out nodes to be used in the data-taking, all nodes: "tpc-01", "tpc-02", "tpc-03", "tpc-04", "tpc-05", "tpc-06", "tpc-07", "tpc-08", "tpc-09", "tpc-10", "tpc-11",
* chambers: list of chambers that will be included in data-taking, all chambers: "VTPC-1", "VTPC-2", "MTPC-L", "MTPC-R", "GTPC", "FTPC-1", "FTPC-2", "FTPC-3", "GRC-L" .

When file was properly adjusted and modified, push the file to gitlab and tell TDAQ coordinator to include the newest file in bookkeeping (configuration file is included in bookkeeping for each event).
Then run:
```
cd /soft/tdaq/readouttools/TPC/Scripts
python3 generate_pedestal_conf_eb.py
```
It will generate config file:
```
/soft/tdaq/readouttools/TPC/Configs/AllTPCs_Pedestals_EB.json
```
Make sure that symbolic link Pedestals.json points to newly created config file.

 

## pteroDAQtyl metrices

The metrices server runs on mon-2 machine (the machine is behind TDAQ firewall - login from supervisor):
```
ssh wojtek@mon-2
```
pswd: wojtek

To restart server run:
```
sudo systemctl status ptero-metrics.service
```
Config file for ptero-metrics daemon process is here: /etc/systemd/system/ptero-metrics.service
