# Network 
![](img/data-network.png)

# Data Switch Occupancy
![](img/occupancy-sw-data-1-2.png)
![](img/occupancy-sw-data-3-sm.png)

# Cabling
## Blade/Node numeration
![](img/c6100_2.png)

## Blade/Node network ports
![](img/c6100_node.png)

## Management & Monitoring network
![](img/event-builders-connection.png)

# Network Switches
## HP 3500
Documentation:

- [Install Guide](pdfs/HP-3500yl-InstallGuide-Dec2005.pdf)
- [Management and Configuration Guide](pdfs/HP-3500_MgmtConfigGde-July2006.pdf)

Those switches are used for management (red cables) and monitoring (blue or green) networks.
### Connecting console cable
To make initial configuration you need normal serial DB-9 RS-232 cable. 
If your computer doesn't have serial port, you have to use USB -> RS-232 converter.
Configure console port to  VT100 (9600 baud 8N1) emulation.

![HP 3500](img/hp-3500-front.png)
![HP 3500](img/hp-3500-back.jpg)

Use prefered serial communication program e.g.
```
minicom -D /dev/ttyS0 -b 9600 -8
```

### Reseting switcih to factory defaults
To execute the factory default reset on the switch, perform these steps:

1. Using pointed objects, simultaneously press both the Reset and Clear buttons on the front of the switch.
2. Continue to press the Clear button while releasing the Reset button.
3. When the Self Test LED begins to blink, release the Clear button.

The switch will then complete its self test and begin operating with its
configuration restored to the factory default settings.

You should see root prompt in minicom similar to this:
```
ProCurve Switch 3500yl-48G#
```

### Configuring
#### Enable SSH
```
configure
ip ssh server enable
ip ssh password-authentication enable
ip ssh rsa-authentication enable 
write memory
exit
```
#### Configure using setup menu
```
menu
```
One would need to setup: 
- system name:
  `Switch Configuration -> System Information`
- IP address and mask:
  `Switch Configuration -> IP Configuration`
- SNMP Communities
  `Switch Configuration -> SNMP Communities`
should be:
```
         Community Name           MIB View  Write Access
--------------------------------  --------  ------------
public                            Manager   Unrestricted
```
- Setup passwords:
  `Console Password -> Set Operator Password`
  `Console Password -> Set Manager Password`
- Setup default VLAN with ID 1:
  `Switch Configuration - VLAN`
  should be:
```
802.1Q VLAN ID                Name
--------------  --------------------------------
1               DEFAULT_VLAN                    
```
- All ports should be untagged:
  `Switch Configuration - VLAN - VLAN Port Assignment`
```
  Port   DEFAULT_VLAN   |  Port   DEFAULT_VLAN
  ---- + ------------   |  ---- + ------------
  1    | Untagged       |  25   | Untagged
  2    | Untagged       |  26   | Untagged
  3    | Untagged       |  27   | Untagged
  4    | Untagged       |  28   | Untagged
  5    | Untagged       |  29   | Untagged
  6    | Untagged       |  30   | Untagged
  7    | Untagged       |  31   | Untagged
  8    | Untagged       |  32   | Untagged
  9    | Untagged       |  33   | Untagged
  10   | Untagged       |  34   | Untagged
  11   | Untagged       |  35   | Untagged
  12   | Untagged       |  36   | Untagged

```

Afterwards exit to CLI (position 5 of the menu)

#### Check with CLI
Enter configuration mode:
```
sw-mon-04# configure
```
Print running configuration.
```
sw-mon-04(config)# show running-config 

Running configuration:

; J8693A Configuration Editor; Created on release #K.15.09.0010
; Ver #03:01.1f.ef:f2
hostname "sw-mon-04"
module 1 type j86yya
module 2 type j86xxa
snmp-server community "public" unrestricted
vlan 1
   name "DEFAULT_VLAN"
   untagged 1-48
   ip address 10.0.0.24 255.255.0.0
   exit
no autorun
no dhcp config-file-update
no dhcp image-file-update
password manager
password operator

```
If you see something like above then you are fine.
Type exit to exit configuration, enable mode and to logout:
```
sw-mon-04(config)# exit
sw-mon-04# exit
sw-mon-04> exit
Do you want to log out [y/n]?
```

## Dell Force10 S60 (1 gbE)
This is sw-data-3 switch used for data flow from slower detectors (DRS based + VD)
### Connecting console cable
You need a machine with serial port or you need USB -> RS-232 converter.
Additionaly you need special DB-9 to RJ-45 adapter like this:
![DB-9 to RJ-45 adapter](img/RS-232-DB9-na-RJ-45.jpeg)
![Dell Force10 S60](img/dell-s60-front.png)

Configure serial communications for 9600,N,8,1 and no flow control.
Use prefered serial communication program e.g.
```
minicom -D /dev/ttyS0 -b 9600 -8
```
### Reseting switcih to factory defaults
- Connect console cable and start communication program
- Power off and on the switch
- During the boot up process, hit any key as soon as you see this: 
`Hit any key to stop autoboot:  0`
- Afterwards execute commands as shown on listing below:
```
Hit any key to stop autoboot:  0 
=> setenv stconfigignore true
=> saveenv
Saving Environment to SD CARD...
done
=> reset
```
- Let it boot completly until you see propmt `FTOS>`
- Restore factory reset as presented below:
```
FTOS>enable
FTOS#restore factory-defaults
FTOS#restore factory-defaults stack-unit all clear-all 

    ***********************************************************************
    *  Warning - Restoring factory defaults will delete the existing      *
    *  startup-config and all persistent settings (stacking, fanout, etc.)*
    *  After restoration the unit(s) will be powercycled immediately.     *
    *  Proceed with caution !                                             *
    ***********************************************************************

Proceed with factory settings? Confirm [yes/no]:yes

-- Restore status --
Unit   Nvram     Config
------------------------
  0   Success   Success   
  1   Not present
  2   Not present
  3   Not present
  4   Not present
  5   Not present
  6   Not present
  7   Not present
  8   Not present
  9   Not present
 10   Not present
 11   Not present

Power-cycling the unit(s).                 


U-Boot 1.3.4 (Dell Force10) (Oct 15 2012 - 05:08:50)
...
```
- The switch will restart. Wait until you see prompt of Bare Metal Provisioning `FTOS-BMP>`
- Now you can start configuring

### Configuring
Enter privileged mode (prompt with `#`) :
```
FTOS-BMP>enable   
```
Exit BMP mode:
```
FTOS-BMP#stop jump-start
% Warning: The jumpStart process will stop ...
...
```
Enter Configure mode and config all 1GbE port to work in access mode (switchport):
```
FTOS#configure
FTOS(conf)#interface range gigabitethernet 0/0 - 49           
% Warning: Non-existing ports (not configured) are ignored by interface-range
FTOS(conf-if-range-gi-0/0-49)#switchport
FTOS(conf-if-range-gi-0/0-49)#no shutdown
00:24:02: %STKUNIT0-M:CP %IFMGR-5-ASTATE_UP: Changed interface Admin state to up: Gi 0/0
00:24:02: %STKUNIT0-M:CP %IFMGR-5-ASTATE_UP: Changed interface Admin state to up: Gi 0/1
  ...
FTOS(conf-if-range-gi-0/0-49)#mtu 9216
```
now check if the interfaces looks like this:
```
FTOS(conf-if-range-gi-0/0-49)#show config
!
interface GigabitEthernet 0/0
 no ip address
 mtu 9216
 switchport
 no shutdown
!
  ...
```
if yes, you can exit:
```
FTOS(conf-if-range-gi-0/0-49)#exit
FTOS(conf)#
```
Now configure management port, which then you can use instead of serial console:
```
FTOS(conf)#interface Managementethernet 0/0
FTOS(conf-if-ma-0/0)#no shutdown
FTOS(conf-if-ma-0/0)#ip address 10.0.0.19/16
FTOS(conf-if-ma-0/0)#exit
```
Setup admin user:
```
FTOS(conf)#username admin privilege 15 password 0 somepassword
FTOS(conf)#enable password level 15 0 somepassword
```
Change hostname:
```
FTOS(conf)#hostname sw-data-x
sw-data-x(conf)#
```
Set reload type from `jump-start` to `normal-reload`:
```
sw-data-x(conf)#reload-type normal-reload 
```
Exit configuration mode:
```
sw-data-x(conf)#exit
sw-data-x#
```

Save configuration:
```
sw-data-x#copy running-config startup-config
!
5526 bytes successfully copied
01:26:57: %STKUNIT0-M:CP %FILEMGR-5-FILESAVED: Copied running-config to startup-config in flash by default

sw-data-x#
```

Show configuration:
```
sw-data-x#show running-config 
Current Configuration ...
! Version 8.3.3.10
! Last configuration change at Thu Nov 10 18:51:38 2022 by default
! Startup-config last updated at Thu Nov 10 18:53:26 2022 by default
!
boot system stack-unit 0 primary system: A:
boot system stack-unit 0 secondary system: B:
boot system stack-unit 0 default system: A:
!
redundancy auto-synchronize full
!
hardware watchdog
!
hostname sw-data-x
!
enable password level 15 7 b125455cf679b2083c51ba54d974dfff716f86751a67d01a
!
username admin password 7 625e6699c4b86e0392000e1ac15f47af privilege 15 
!
stack-unit 0 provision S60
!
interface GigabitEthernet 0/0
 no ip address
 mtu 9216
 switchport
 no shutdown
!
interface GigabitEthernet 0/1
 no ip address
 mtu 9216
 switchport
 no shutdown
!
  ...
!
interface GigabitEthernet 0/47
 no ip address
 mtu 9216
 switchport
 no shutdown
!
interface TenGigabitEthernet 0/48
 no ip address
 shutdown
!
interface TenGigabitEthernet 0/49
 no ip address
 shutdown
!
interface ManagementEthernet 0/0
 ip address 192.168.1.222/24
 no shutdown
!
interface ManagementEthernet 1/0
 no shutdown
!
interface ManagementEthernet 2/0
 no shutdown
!
interface ManagementEthernet 3/0
 no shutdown
!
interface ManagementEthernet 4/0
 no shutdown
!
interface ManagementEthernet 5/0
 no shutdown
!
interface ManagementEthernet 6/0
 no shutdown
!
interface ManagementEthernet 7/0
 no shutdown
!
interface ManagementEthernet 8/0
 no shutdown
!
interface ManagementEthernet 9/0
 no shutdown
!
interface ManagementEthernet 10/0
 no shutdown
!
interface ManagementEthernet 11/0
 no shutdown
!
interface Vlan 1
!untagged GigabitEthernet 0/0-47
!
ip ssh server enable
!
line console 0
line vty 0
line vty 1
line vty 2
line vty 3
line vty 4
line vty 5
line vty 6
line vty 7
line vty 8
line vty 9
!
end
```

Restart switch:
```
sw-data-x#reload

Proceed with reload [confirm yes/no]: yes
syncing disks... 1 done
  ...
```

After reboot, try to SSH to it via management port (just next to RS-232 console port).
Probably you will need to enable `3des-cbc` cipher and  `diffie-hellman-group1-sha1` key exchange method
or use ssh with the following options:
```
ssh -c3des-cbc -oKexAlgorithms=+diffie-hellman-group1-sha1 admin@10.0.0.19
```

**TO BE ADDED:**

- Configuration of SNMP server




## Juniper QFX-5200 (100 gbE)
Main data switches, sw-data-1, sw-data-2, working together as master/slave providing redundancy.
Swithc sw-data is an alias to the current master.
### Connecting console cable
Same as with Dell switches, you need a machine with serial port or you need USB -> RS-232 converter.
Additionaly you need special DB-9 to RJ-45 adapter like this:
![DB-9 to RJ-45 adapter](img/RS-232-DB9-na-RJ-45.jpeg)
![Console cable connection](img/juniper-mgmt-connection.png)

Use prefered serial communication program e.g.
```
minicom -D /dev/ttyS0 -b 9600 -8
```
### Reseting switch
#### To revert the switch to the rescue configuration (IF YOU HAVE PASSWORD):
```
[edit]
user@switch# load factory-default
[edit]
user@switch# delete system commit factory-settings
[edit]
user@switch# commit
```
## To revert to the factory-default configuration by using the Factory Reset/Mode button:
1. Press the Factory Reset/Mode button for 10 seconds. The switch transitions into factory-default configuration and the console displays committing
factory default configuration.
2. Press the Factory Reset/Mode button for 10 more seconds. The switch transitions into initial setup mode and the console displays committing
ezsetup config.


### Configuring
