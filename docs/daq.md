
# Access
## Web interface
The web interface can be accessed from CERN GPN network
with the following page:
[WebPage](https://na61-tdaq-gw3.cern.ch)

## Development Access
Example access to supervisor:
```
ssh -J na61-tdaq-gw3.cern.ch supervisor
```
Available TDAQ gateways:

* na61-tdaq-gw1.cern.ch
* na61-tdaq-gw2.cern.ch
* na61-tdaq-gw3.cern.ch
* na61-tdaq-gw4.cern.ch



# Design
## Network Topology
![topology](img/dataflow-topology.png)

## Data flow 
- estimated size of uncompressed Pb+Pb event ~ 10MB/s

![topology](img/dataflow-links.png)

## Data flow software
![topology](img/dataflow-software.jpg)

## Numbers

### Sources
- [TDAQ status 2nd July 2020](pdfs/TDAQ_status_2nd-July-2020.pdf)
- [DRS estimates](pdfs/drs-estimates.pdf)
- [TPC estimates](pdfs/2020_Reconstruction_Software_-_CERN_-_2-11-20.pdf)

---
### Event size
Estimated sizes for TPC, **uncompressed** (Brant):

- Pb+Pb 10 MB +/- 4 MB

Estimated sizes for TPC, **compressed** (Brant):
- Pb+Pb 1.4 MB

Estimations for DRS:

- Event size (without TPC): 1.1 MB/s
- Data rate: 8.5 Gb/s



---
### Online compression
- Time limit:  5.76 s/event
- Typically processing time 0.5 - 5 s/event
- Online clusterization 
    - 1GB/chunk → 0.42 GB/chunk (Pb+Pb@150GeV/c)
    - 1GB/chunk → 0.11 GB/chunk (p+C@31GeV/c)
- Noise cluster filtering
    - 1GB/chunk → 0.16 GB/chunk (Pb+Pb@150GeV/c)
    - 1GB/chunk → 0.005 GB/chunk (p+C@31GeV/c)


---
### Buffers
Disk raid I/O bandwith (single buffer):

- `24 hdds * 100 MB/s = 2.4 GB = 19.2 Gb/s`

Average including duty cycle:

- `19.2 Gb/s * 3 = 57.6 Gb/s `
