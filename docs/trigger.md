# Trigger


## Technical documentation

### Schematics of Trigger distribution modules

PCB schematics of the following modules are available:

* [Main distribution module (top PCB)](pdfs/TDB_TOPv12.pdf)
* [Main distribution module (bottom PCB)](pdfs/TDB_BOTTOM_v11.pdf)
* [TriggerBox](pdfs/TD_Box.pdf)

### Registers of the main trigger module (CAEN V2495)

* [V2495_registers](pdfs/V2495_registers.pdf)

## Signal mapping

### Scalers

| Range |       Signal name        | Comment | 
| ----- | ------------------------ | ------- |
| 0-15  | inputs from CFD          | -       |
| 16-19 | T1-T4 triggers           | -       |
| 20-23 | T1-T4 triggers pre.      | -       |
| 24    | L0 trigger               | -       |
| 25    | L1 trigger               | -       |
| 26    | L2 trigger               | -       |
| 27    | L0 trigger gated         | -       |
| 28    | L1 trigger gated         | -       |
| 29    | CLEAR	                   | -       |
| 30-33 | T1-T4 triggers gtd       | -       |
| 34-37 | T1-T4 triggers pre. gtd. | -       |
| 38-39 | -                        | -       |

### MHTDC

| Range | Signal name              | Comment | 
| ----- | ------------------------ |-------- |
| 0     | L0 trigger               | -       |
| 1     | L1 trigger               | -       |
| 2     | L2 trigger               | -       |
| 3-6   | T1-T4 triggers           | -       |
| 7-10  | T1-T4 triggers prescaled | -       |
| 11    | TPC phase                | -       |
| 12    | CLEAR                    | -       |
| 13-15 | -                        | -       |
| 16-31 | inputs from CFD          | -       |

### Pattern unit

| Range |       Signal name        | Comment | 
| ----- | ------------------------ | ------- |
| 0-3   | T1-T4 triggers           | -       |
| 4-7   | T1-T4 triggers prescaled | -       |
| 8-15  | -                        | -       |
| 31-16 | inputs from CFD          | -       |
| 32-63 | -                        | -       |


### Output signals

| Range |       Signal name        | Comment           | 
| ----- | ------------------------ | --------          |
| 0-15  | inputs from CFD          | clock sync.       |
| 16-19 | T1-T4 triggers           | -                 |
| 20-23 | T1-T4 triggers prescaled | -                 |
| 24    | L0 trigger               | -                 |
| 25    | L1 trigger               | -                 |
| 26    | L2 trigger               | -                 |
| 27    | L0 trigger gated         | -                 |
| 28    | L1 trigger gated         | -                 |
| 29    | TPC phase                | -                 |
| 30    | TTC channel A            | -                 |
| 31    | TTC channel B            | -                 |
| 32-40 | sub-detector BUSY        | from TDM          |
| 41-42 | TPC BUSY                 | only first in use |
| 43    | global BUSY              | -                 |
| 44-59 | inputs from CFD          | asynchronous      |
| 60    | CLEAR                    | -                 |
