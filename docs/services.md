# Services

# TDAQ Core & TDAQ WebApp
[Git repository for TDAQ WebApp](https://gitlab.cern.ch/na61-software/online/TDAQ/tdaq-control-webapp-v3)

[Git repository for TDAQ Core](https://gitlab.cern.ch/na61-software/online/TDAQ/tdaq-control-core-v3)

# Trigger Monitoring Proxy
[Trigger proxy repository](https://gitlab.cern.ch/owyszyns/TDAQTriggerMonProxy)

# Bookkeeping
[Bookkeeping repository](https://gitlab.cern.ch/owyszyns/bookkeeping)

[Bookkeeping Connector repository](https://gitlab.cern.ch/na61-software/online/TDAQ/bk-tdaq-connector)
[HTTP Commander repository](https://gitlab.cern.ch/owyszyns/http-commander)


# OnlineQA
[OnlineQA repository](https://gitlab.cern.ch/owyszyns/tdaq-online-qa)
The OnlineQA works in the following manner:

- pteroDAQtyl qa module is running within every Event Builder process.
- this module pteroDAQtyl qa module is serving everyt n-th event on standard network socket.
  Sending interval (typically every 1000 events) and network port (7000) is defined in TDAQ run configuration.
- on every running Event Building machine, there is a systemd service running Shine instance
by executing `/soft/tdaq/online-qa/startOnlineQA.sh` script. Path `/soft` is a mount path for TDAQ NFS filesystem.
In normal condition, the NFS server is located at supervisor machine and exports the same path `/soft`.
For the time being, this service is running constantly and in case of failure, it is automaticaly restarted by systemd.
Operations of starting, stoping and restarting of Shine instance on all machines is permormed via Ansible playbooks.
- after Shine onlineQA module processes configurable number of events, it sends histograms to `qa-1` machine on port `54321`,
where a server (`/home/qa/webApp/tools/server.py`) receives and processes histograms from all machines and saves to database (mongoDB).
- when historgrams hit database, one can use onlineQA web interface to browse content of this database in userfriedly manner.

## Shine instance location
### Testbed
It is located in home directory of `qa` user at `qa-1` machine:

- `~/onlineQA` is the Shine application
- `~/shine` this directory holds Shine installation

And it is executed at `qa-1` machine.
The input file section of `/home/qa/onlineQA/EventFileReader.xml` file should point to virtual event builder e.g. veb-002:
```
  <InputFilenames>
	  pterawserver:veb-002:7000
  </InputFilenames>
```


### Production
It is located in `/soft/tdaq` directory of `tdaq` user at `supervisor` machine:

- `/soft/tdaq/online-qa` is the Shine application
- `/soft/tdaq` this directory holds Shine installation

In case of update, after testing it on TestBed, one has to update mentioned above installation on `supervisor` machine
and restart Shine instances on all Event Builders using Ansible.


# OfflineQA
OfflineQA is an shell script run remotely on na61qa@lxplus by archiver-cta.sh 
script after a file has been transfered to CTA successfully.

# Archiver V1
First version of the Archiver is set of scripts managed by incron daemon.

# DCS
DCS is an external service monitored by TDAQ Core watchdog named "dcs".

# Electronic Logbook (eLog)
New eLog is under development.
Old eLog is available at [na61-elog.cern.ch](https://na61-elog.cern.ch).
