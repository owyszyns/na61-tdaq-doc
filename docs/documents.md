
## Event Structure

* Full event:
    + [Full event tructure](pdfs/event/RawDataFormat.pdf)
    + [Example event with decoder](https://cernbox.cern.ch/index.php/s/JubeL7Q7JLqSSTJ)

* Sub-events:
    + [Trigger SubEvent](pdfs/event/triggerFormat.pdf)
    + [TPC SubEvent format](pdfs/event/tpcFormat.pdf), [TPC SubEvent example](https://cernbox.cern.ch/index.php/s/HVZuh7k4oVmZT2k)
    + [DRS4 SubEvent](pdfs/event/drs_data_structure.pdf), [DRS SubEvent decoder - EventReader.cpp](https://gitlab.cern.ch/na61-software/online/readouttools/-/tree/master/DRS/tools), [DRS SubEvent example](https://cernbox.cern.ch/index.php/s/VhIaV5sD3RaIpoW)
    + [VD SubEvent](pdfs/event/vdFormat.pdf)



## Technical Design Reports

- [TDAQ](pdfs/tdaq-tdr.pdf)
- [Trigger](pdfs/trigger-tdr.pdf)
- [TPC](pdfs/tpc-tdr.pdf)


# TPC stuff

## TPC FEE documentation (from ALICE)

- [ALTRO manual](tpc/UserManual_draft_02.pdf)
- [FEC Schematics](tpc/FEC_Sch.pdf)
- [FEC Back-end connectors pinout](tpc/FEC_Backend_connmap.pdf)
- [FEC Input connectors mapping](tpc/FEC_Input_connmap.pdf)
- [Definition of the internal FEC signals](tpc/FEC_Signals.pdf)
- [FEC PCB stack-up](tpc/FEC_PCB_StackUp.pdf)
- [FEC PCB floorplan](tpc/FEC_floorplan.pdf)
- [FEC Bill of material](tpc/FEC_BOM_FEC40021090.pdf)
- [ALTRO BUS](tpc/altro_bus.zip)

## Gating pulser

- [User manual](tpc/user_manual_GP_6.pdf)