# Serving page locally

## Install MkDocs
```
sudo apt install python3-venv
python3 -m venv venv
. venv/bin/activate
pip install mkdocs mkdocs-bootstrap mkdocs-material mkdocs-material-extensions
```

## Serving local instance
assuming that the cloned repository is located in `na61-tdaq-doc` directory:
```
cd na61-tdaq-doc
mkdocs serve
```
This command will produce an URL link which should be opened in a web browser.
Any file change will trigger web page reload.
